﻿// App1.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
// 测试程序，弹出弹窗
// 可以新增部分功能以备检测，如创建注册表等

#include <iostream>
#include <winsock2.h>
#include <windows.h>
#include <ws2tcpip.h>
#pragma comment(lib, "ws2_32.lib")

#define PAGE_SIZE 4096

// 测试弹窗（任务1）
void testMBox();

// 测试堆操作（任务2）
void testHeapOP();

// 测试文件操作（任务3）
int testFileOP();

//测试注册表操作（任务4）
void testRegCreateAndSetValue();
void testRegOpenAndDelValue();

//测试堆操作异常行为分析（任务5）


// 测试文件异常行为（任务6）
int testFileExceptions();

// 测试网络通信操作（任务13）
void testNetworkOP();

//测试堆操作异常行为分析（任务5）
void headRepeatedRelease();

int main()
{


    // system("pause");
   //testMBox();
   //testHeapOP();
   //testFileOP();

    //↓注册表测试用↓
    //testRegCreateAndSetValue();
    //testRegOpenAndDelValue();
    //↑注册表测试用↑
    // testFileExceptions();

    testNetworkOP();

    headRepeatedRelease();

    // system("pause");
    return 0;
}

// 测试弹窗（任务1）
void testMBox() {
    MessageBoxA(NULL, "MessageBoxA: App for test", NULL, NULL);
    MessageBoxW(NULL, L"MessageBoxW: App for test", NULL, NULL);
}

// 测试堆操作（任务2）
void testHeapOP()
{
    HANDLE hCustomHeap = HeapCreate(0, 0, 0);
    if (hCustomHeap)
    {
        LPVOID pHeapAllocated = HeapAlloc(hCustomHeap, 0, 1024);
        if (pHeapAllocated) {
            ZeroMemory(pHeapAllocated, 1024); // 使用分配的内存
            HeapFree(hCustomHeap, 0, pHeapAllocated); // 释放分配的内存
        }
        HeapDestroy(hCustomHeap);
    }
}

// 测试文件操作（任务3）
int testFileOP() {
    // 创建文件
    HANDLE hFile = CreateFile(
        L"example.txt",                            // 文件名
        GENERIC_READ | GENERIC_WRITE,              // 访问权限（读写）
        FILE_SHARE_READ,                           // 共享模式
        NULL,                       // 安全属性（默认）
        CREATE_ALWAYS,              // 文件创建方式（如果存在则覆盖）
        NULL,                       // 文件属性
        NULL                        // 模板句柄（不使用）
    );

    if (hFile == INVALID_HANDLE_VALUE)
    {
        // 创建文件失败
        printf("Failed to create file.\n");
        return 1;
    }

    // 写入数据到文件
    const char* data = "Hello, World!";
    DWORD bytesWritten;
    bool flag = WriteFile(hFile, data, strlen(data), &bytesWritten, NULL);
    if (!flag)
    {
        // 写入文件失败
        printf("Failed to write to file.\n");
        CloseHandle(hFile); // 关闭文件句柄
        return 1;
    }
    FlushFileBuffers(hFile);

    // 关闭文件句柄
    CloseHandle(hFile);
    
    // 重新打开文件以供读取
    hFile = CreateFile(
        L"example.txt",             // 文件名
        GENERIC_READ,               // 访问权限（读取）
        0,                          // 共享模式（无共享）
        NULL,                       // 安全属性（默认）
        OPEN_EXISTING,              // 文件打开方式（只打开已存在的文件）
        FILE_ATTRIBUTE_NORMAL,      // 文件属性（普通文件）
        NULL                        // 模板句柄（不使用）
    );

    if (hFile == INVALID_HANDLE_VALUE)
    {
        // 打开文件失败
        printf("Failed to open file for reading.\n");
        return 1;
    }

    // 读取文件内容
    char buffer[100];
    DWORD bytesRead;
    if (!ReadFile(hFile, buffer, sizeof(buffer), &bytesRead, NULL))
    {
        // 读取文件失败
        printf("Failed to read file.\n");
        CloseHandle(hFile); // 关闭文件句柄
        return 1;
    }

    // 关闭文件句柄
    CloseHandle(hFile);
    
    return 0;
}

//测试注册表操作（任务4）
void testRegCreateAndSetValue() {
    // 创建注册表并设置键值
    HKEY hKey = NULL;
    TCHAR Data[254];
    memset(Data, 0, sizeof(Data));
    wcsncpy_s(Data, TEXT("xf_text_random"), 254);

    size_t lRet = RegCreateKeyEx(HKEY_CURRENT_USER, (LPWSTR)L"ThisMykey", 0, NULL, REG_OPTION_NON_VOLATILE,
        KEY_ALL_ACCESS, NULL, &hKey, NULL);
    if (lRet == ERROR_SUCCESS) {
        printf("create successfully!\n");
    }
    else {
        printf("failed to create!\n");
    }
    // 修改注册表键值，没有则创建它
    size_t iLen = wcslen(Data);
    // 设置键值
    lRet = RegSetValueEx(hKey, L"CrazyThursDay", 0, REG_SZ, (CONST BYTE*)Data, sizeof(TCHAR) * iLen);
    if (lRet == ERROR_SUCCESS)
    {
        printf("set value successfully!\n");
        return;
    }
    else {
        printf("failed to set value!\n");
    }
    RegCloseKey(hKey);
}
void testRegOpenAndDelValue() {
    HKEY hKey = NULL;
    size_t lRet = RegOpenKeyEx(HKEY_CURRENT_USER, (LPWSTR)L"ThisMykey", 0, KEY_ALL_ACCESS, &hKey);
    if (lRet == ERROR_SUCCESS) {
        printf("open successfully!\n");
    }
    else {
        printf("open failed\n");
    }
    lRet = RegDeleteValue(hKey, L"panfeng");
    if (lRet == ERROR_SUCCESS) {
        printf("delete success!\n");
    }
    else {
        printf("delete fail!\n");
    }
    RegCloseKey(hKey);
}

//测试堆操作异常行为分析（任务5）
void headRepeatedRelease() {

    HANDLE hHeap = HeapCreate(HEAP_NO_SERIALIZE, PAGE_SIZE * 10, PAGE_SIZE * 100);

    int* pArr = (int*)HeapAlloc(hHeap, 0, sizeof(int) * 30);
    for (int i = 0; i < 30; ++i)
    {
        pArr[i] = i + 1;
    }
    HeapFree(hHeap, 0, pArr);
    HeapFree(hHeap, 0, pArr);
    HeapDestroy(hHeap);
}

// 测试文件异常行为（任务6）
int testFileExceptions()
{
    // #1: 修改了其它可执行代码包括 exe，dll，ocx
    HANDLE hFile = CreateFile(
        L"example.exe",                            // 文件名
        GENERIC_WRITE,              // 访问权限（读写）
        FILE_SHARE_READ,                           // 共享模式
        NULL,                       // 安全属性（默认）
        CREATE_ALWAYS,              // 文件创建方式（如果存在则覆盖）
        NULL,                       // 文件属性
        NULL                        // 模板句柄（不使用）
    );

    if (hFile == INVALID_HANDLE_VALUE)
    {
        // 创建文件失败
        printf("Failed to create file.\n");
        return 1;
    }
    CloseHandle(hFile);

    // #2: 存在自我复制的情况
    // 获取当前文件名
    wchar_t lpFileName[MAX_PATH];
    GetModuleFileNameW(NULL, lpFileName, MAX_PATH);
    std::wstring wideStr(lpFileName);
    std::string fullName(wideStr.begin(), wideStr.end());
    std::string dirName, fileName;
    // 找到最后一个反斜杠的位置
    size_t lastBackslashPos = fullName.find_last_of('\\');
    if (lastBackslashPos != std::string::npos) {
        // 提取文件名
        fileName = fullName.substr(lastBackslashPos + 1);
    }
    else {
        // 如果没有反斜杠，则整个路径都是文件名
        fileName = fullName;
    }
    // 复制当前程序到D盘
    // 即复制.\\App1.exe ----> D:\\App1.exe

    std::wstring wFileName = L"D:\\" + std::wstring(fileName.begin(), fileName.end());
    // 新建文件
    hFile = CreateFile(
        wFileName.c_str(),             // 文件名
        GENERIC_READ,               // 访问权限（读取）
        0,                          // 共享模式（无共享）
        NULL,                       // 安全属性（默认）
        CREATE_ALWAYS,              // 文件打开方式（只打开已存在的文件）
        FILE_ATTRIBUTE_NORMAL,      // 文件属性（普通文件）
        NULL                        // 模板句柄（不使用）
    );

    if (hFile == INVALID_HANDLE_VALUE)
    {
        // 打开文件失败
        printf("Failed to open file for reading.\n");
        return 1;
    }
    // 关闭文件句柄
    CloseHandle(hFile);

    return 0;
}


// 测试网络通信操作（任务13）
void testNetworkOP()
{
    // 初始化 Winsock
    WSADATA wsaData;
    WSAStartup(MAKEWORD(2, 2), &wsaData);

    // 创建套接字
    SOCKET serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    // 绑定套接字到特定的IP地址和端口
    sockaddr_in serverAddr;
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = htons(12345);  // 设置端口号
    inet_pton(AF_INET, "1.2.3.4", &(serverAddr.sin_addr));
    bind(serverSocket, (struct sockaddr*)&serverAddr, sizeof(serverAddr));

    SOCKET clientSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

    // 发送数据到客户端
    const char* message = "Hello, client!";
    send(clientSocket, message, strlen(message), 0);
    // 接收来自客户端的数据
    char buffer[1024];
    int bytesRead = recv(clientSocket, buffer, sizeof(buffer), 0);

    // 关闭套接字和清理 Winsock
    closesocket(clientSocket);
    closesocket(serverSocket);
    WSACleanup();
}
