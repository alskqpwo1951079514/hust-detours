import sys
from PyQt6.QtWidgets import QApplication, QMainWindow, QPushButton, QVBoxLayout, QWidget, QLineEdit, QTextEdit, QLabel, QHBoxLayout
from PyQt6.QtCore import QThread, pyqtSignal
import subprocess

class InjectorThread(QThread):
    output = pyqtSignal(str)
    file_action_output = pyqtSignal(str)  # 新增信号用于单独处理文件操作异常行为的输出

    def __init__(self, exe_path, dll_path, app_path, parent=None):
        super().__init__(parent)
        self.exe_path = exe_path
        self.dll_path = dll_path
        self.app_path = app_path

    def run(self):
        try:
            process = subprocess.Popen(self.exe_path, stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                                       stderr=subprocess.STDOUT, text=True,cwd="./DllInjector")
            process.stdin.write(f"{self.dll_path}\n{self.app_path}\n")
            process.stdin.close()

            for line in process.stdout:
                if line.startswith("！"):  # 判断是否是文件操作异常行为输出
                    self.file_action_output.emit(line.rstrip())  # 发射文件操作异常行为的信号
                else:
                    self.output.emit(line.rstrip())

            process.wait()  # 等待注入器程序执行完成
            print("")
        except Exception as e:
            print(f"错误: {e}")
        finally:
            self.output.emit("")


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()

        self.setWindowTitle("DLL注入器")
        self.setGeometry(100, 100, 600, 400)

        self.dll_label = QLabel("dll文件路径")
        self.dll_line_edit = QLineEdit()

        self.app_label = QLabel("测试app路径")
        self.app_line_edit = QLineEdit()

        self.output_textedit = QTextEdit()
        self.output_textedit.setReadOnly(True)

        self.file_action_output_textedit = QTextEdit()
        self.file_action_output_textedit.setReadOnly(True)  # 设置只读

        self.inject_button = QPushButton("开始", self)
        self.inject_button.clicked.connect(self.run_dll_injector)
        self.inject_button.setFixedWidth(80)  # 设置按钮宽度为 80 像素
        self.inject_button.setFixedHeight(30)  # 设置按钮高度为 30 像素

        self.clear_button = QPushButton("清屏", self)
        self.clear_button.clicked.connect(self.clear_output)
        self.clear_button.setFixedWidth(80)  # 设置按钮宽度为 80 像素
        self.clear_button.setFixedHeight(30)  # 设置按钮高度为 30 像素

        # 创建水平布局，放置注入和清屏按钮
        button_layout = QHBoxLayout()
        button_layout.addWidget(self.inject_button)
        button_layout.addWidget(self.clear_button)

        # 创建垂直布局，放置输入框、按钮和输出窗口
        main_layout = QVBoxLayout()
        main_layout.addWidget(self.dll_label)
        main_layout.addWidget(self.dll_line_edit)
        main_layout.addWidget(self.app_label)
        main_layout.addWidget(self.app_line_edit)
        main_layout.addLayout(button_layout)
        main_layout.addWidget(self.output_textedit)

        # 创建垂直布局，放置文件操作异常行为的输出窗口
        file_action_layout = QVBoxLayout()
        file_action_layout.addWidget(QLabel("异常行为输出"))
        file_action_layout.addWidget(self.file_action_output_textedit)

        # 创建水平布局，将主布局和文件操作异常行为布局放在一起
        combined_layout = QHBoxLayout()
        combined_layout.addLayout(main_layout)
        combined_layout.addLayout(file_action_layout)

        main_widget = QWidget()
        main_widget.setLayout(combined_layout)

        self.setCentralWidget(main_widget)

    def run_dll_injector(self):
        exe_path = "./DllInjector/DllInjector.exe"
        print("开始执行注入器程序：", exe_path)  # 添加调试信息

        dll_path = self.dll_line_edit.text()
        app_path = self.app_line_edit.text()

        self.thread = InjectorThread(exe_path, dll_path, app_path)
        self.thread.output.connect(self.append_output)
        self.thread.file_action_output.connect(self.append_file_action_output)  # 连接文件操作异常行为的输出信号
        self.thread.start()

    def append_output(self, text):
        self.output_textedit.append(text)

    def append_file_action_output(self, text):
        self.file_action_output_textedit.append(text)

    def clear_output(self):
        self.output_textedit.clear()
        self.file_action_output_textedit.clear()  # 清空文件操作异常行为的输出窗口


if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    sys.exit(app.exec())
