# 基于API HOOK的软件行为分析系统的使用文档

## 1. 项目介绍

项目主要分为几个部分：

（1）Dll目录：存放生成dll文件的工程代码，定义了替换函数

（2）DllInjector目录：存放dll注射器的工程代码，负责将dll文件注入测试程序，替换相关的API

（3）App目录：行为检测样本库，测试程序负责调用测试相关的API

（4）docs和img目录：文档说明

（5）pyqtui.py代码文件：图形界面

## 2. 环境配置

> 本项目使用VS 2022开发，所有程序均在x86下编译。

### 2.1 克隆仓库

```shell
git clone git@gitee.com:cronyaium/hust-detours.git
```

### 2.2 编译 Detours 库

首先，运行`x86 Native Tools Command Prompt for VS 2022`

进入仓库目录后，执行命令

```powershell
nmake /f Makefile
```

仓库目录下成功生成`lib.X86`、`bin.X86`以及`include`目录，说明Detours库编译成功。

### 2.3 编译生成dll、dll注射器和被注入程序

使用VS打开`Dll/Dll.sln`，使用`Debug x86`编译，生成解决方案，生成`Dll.dll`。

使用VS打开`App/App1/App1.sln`，使用`Debug x86`编译，生成解决方案，生成`App1.exe`。

使用VS打开`DllInjector/DllInjector/DllInjector.sln`，使用`Debug x86`编译，生成解决方案，生成`DllInjector.exe`，复制一份到DllInjector的项目目录下。

> 注意：如果Detours库未找到，需要在“调试属性>V/C++目录”下添加“包含目录”和“库目录”
>
> “包含目录”添加仓库目录下的include目录
>
> “库目录”添加仓库目录下的lib.X86目录

### 2.4 配置python环境

本项目使用的`PyQt6`库要求python版本不低于3.6.1。

使用`pip`安装`PyQt6`库：

```powershell
pip install PyQt6
```

在仓库目录下，使用`python`运行图形界面：

```powershell
python pyqtui.py
```

成功打开图形界面后，说明环境配置成功。

## 3. 效果展示
![UI界面](../img/ui.png)


## 4. 设计思路

### 4.1 整体框架

![框架图](../img/design_p1.png)

### 4.2 HOOK API说明

依照课程设计指导书，API类型可以分为WindowsAPI、堆操作API、文件操作API截获、注册表操作API、网络通信操作API。
对应的具体`DetourAttach`如下：
```cpp
DetourAttach(&(PVOID&)OldMessageBoxW, NewMessageBoxW);
DetourAttach(&(PVOID&)OldMessageBoxA, NewMessageBoxA);
DetourAttach(&(PVOID&)OldCreateFile, NewCreateFile);
DetourAttach(&(PVOID&)OldWriteFile, NewWriteFile);
DetourAttach(&(PVOID&)OldReadFile, NewReadFile);
DetourAttach(&(PVOID&)OldHeapCreate, NewHeapCreate);
DetourAttach(&(PVOID&)OldHeapDestory, NewHeapDestory);
DetourAttach(&(PVOID&)OldHeapFree, NewHeapFree);
DetourAttach(&(PVOID&)OldRegCreateKeyEx, NewRegCreateKeyEx);
DetourAttach(&(PVOID&)OldRegSetValueEx, NewRegSetValueEx);
DetourAttach(&(PVOID&)OldRegDeleteValue, NewRegDeleteValue);
DetourAttach(&(PVOID&)OldRegCloseKey, NewRegCloseKey);
DetourAttach(&(PVOID&)OldRegOpenKeyEx, NewRegOpenKeyEx);
DetourAttach(&(PVOID&)Oldsocket, Newsocket);
DetourAttach(&(PVOID&)Oldbind, Newbind);
DetourAttach(&(PVOID&)Oldsend, Newsend);
DetourAttach(&(PVOID&)Oldconnect, Newconnect);
DetourAttach(&(PVOID&)Oldrecv, Newrecv);
```

### 4.3 异常行为分析

异常行为分析具体包括堆操作异常行为分析、文件操作异常行为分析、注册表操作异常行为分析。

**堆操作异常行为分析包括：**

（1）检测堆申请与释放是否一致（正常）；

（2）是否发生重复的多次释放（异常）

**文件操作异常行为分析包括：**

（1）判断操作范围是否有多个文件夹；

（2）是否存在自我复制的情况；

（3）是否修改了其它可执行代码包括exe，dll，ocx等

**注册表操作异常行为分析包括：**

（1）判断是否新增注册表项并判断是否为自启动执行文件项；

（2）是否修改了注册表

### 4.4 行为检测样本设计

设计样本时主要围绕着测试弹窗、堆操作、文件操作、注册表操作、网络通信操作以及各种异常分析行为展开，且能够保证在设计的界面上输出需要的所有参数信息。