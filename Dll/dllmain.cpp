﻿// dllmain.cpp : 定义 DLL 应用程序的入口点。
//add sth
#include "pch.h"
#include "framework.h"
#include "detours.h"
#include "stdio.h"
#include "stdarg.h"
#include <ws2tcpip.h>
#include <winsock2.h>
#include "windows.h"
#include <iostream>
#include <string>

enum TYPE {
    FUNC_MESSAGEBOXW,
    FUNC_MESSAGEBOXA,
    FUNC_HEAPCREATE,
    FUNC_HEAPDESTROY,
    FUNC_HEAPFREE,
    FUNC_CREATEFILE,
    FUNC_OPENFILE,
    FUNC_READFILE,
    FUNC_WRITEFILE,
    FUNC_CLOSEHANDLE ,
    FUNC_REGCREATEKEYEX ,
    FUNC_REGSETVALUEEX ,
    FUNC_REGCLOSEKEY ,
    FUNC_REGOPENKEYEX ,
    FUNC_REGDELETEVALUE,
    FUNC_SOCKET,
    FUNC_BIND,
    FUNC_SEND,
    FUNC_CONNECT,
    FUNC_RECV
};


#pragma comment(lib, "detours.lib")
#pragma comment(lib, "ws2_32.lib")

// 结构体，存放需要输出的日志信息
struct info {
    int type;                      // 类型：用于区分是哪个函数发出的日志
    int argNum;                    // 参数个数：与下面的两个字符数组的第一维对应，小于10
    SYSTEMTIME st;                 // 系统时间
    char argName[10][30] = { 0 };  // 参数的名称
    char argValue[10][70] = { 0 }; // 参数的值
};


SYSTEMTIME st;

info sendInfo;
HANDLE hReadSemaphore = OpenSemaphore(EVENT_ALL_ACCESS, FALSE, L"ReadSemaphore");  // 信号量
HANDLE hWriteSemaphore = OpenSemaphore(EVENT_ALL_ACCESS, FALSE, L"WriteSemaphore");  // 信号量
HANDLE hMapFile = OpenFileMapping(FILE_MAP_ALL_ACCESS, NULL, L"ShareMemory"); // 共享内存
LPVOID lpBase = MapViewOfFile(hMapFile, FILE_MAP_ALL_ACCESS, 0, 0, sizeof(info));

/*** 任务1：处理 MessageBox 的拦截 ***/
// 定义和引入需要Hook的函数，和替换的函数
static int (WINAPI* OldMessageBoxW)(_In_opt_ HWND hWnd, _In_opt_ LPCWSTR lpText, _In_opt_ LPCWSTR lpCaption, _In_ UINT uType) = MessageBoxW;
static int (WINAPI* OldMessageBoxA)(_In_opt_ HWND hWnd, _In_opt_ LPCSTR lpText, _In_opt_ LPCSTR lpCaption, _In_ UINT uType) = MessageBoxA;

extern "C" __declspec(dllexport)int WINAPI NewMessageBoxA(_In_opt_ HWND hWnd, _In_opt_ LPCSTR lpText, _In_opt_ LPCSTR lpCaption, _In_ UINT uType)
{
    sendInfo.type = FUNC_MESSAGEBOXA;
    GetLocalTime(&sendInfo.st);
    // 需要输出四个参数
    sendInfo.argNum = 4;
    // 参数名称
    sprintf_s(sendInfo.argName[0], "hWnd");
    sprintf_s(sendInfo.argName[1], "lpText");
    sprintf_s(sendInfo.argName[2], "lpCaption");
    sprintf_s(sendInfo.argName[3], "uType");

    // 参数值
    sprintf_s(sendInfo.argValue[0], "%08X", hWnd);
    // 注意判断是否为NULL
    if (lpText != NULL)
    {
        sprintf_s(sendInfo.argValue[1], lpText);
    }
    if (lpCaption != NULL)
    {
        sprintf_s(sendInfo.argValue[2], lpCaption);
    }
    sprintf_s(sendInfo.argValue[3], "%08X", uType);

    // 把需要发送的信息复制到共享内存中
    WaitForSingleObject(hWriteSemaphore, INFINITE);
    memcpy(lpBase, &sendInfo, sizeof(info));
    // V操作 信号量+1 使得注射器能够获取共享内存中的信息
    ReleaseSemaphore(hReadSemaphore, 1, NULL);
    return OldMessageBoxA(hWnd, lpText, lpCaption, uType);
}

extern "C" __declspec(dllexport)int WINAPI NewMessageBoxW(_In_opt_ HWND hWnd, _In_opt_ LPCWSTR lpText, _In_opt_ LPCWSTR lpCaption, _In_ UINT uType)
{
    sendInfo.type = FUNC_MESSAGEBOXW;
    GetLocalTime(&sendInfo.st);
    // 需要输出四个参数
    sendInfo.argNum = 4;
    // 参数名称
    sprintf_s(sendInfo.argName[0], "hWnd");
    sprintf_s(sendInfo.argName[1], "lpText");
    sprintf_s(sendInfo.argName[2], "lpCaption");
    sprintf_s(sendInfo.argName[3], "uType");

    // 参数值
    sprintf_s(sendInfo.argValue[0], "%08X", hWnd);
    if (lpText != NULL)
    {
        int num = WideCharToMultiByte(CP_OEMCP, NULL, lpText, -1, NULL, 0, NULL, FALSE);
        WideCharToMultiByte(CP_OEMCP, NULL, lpText, -1, sendInfo.argValue[1], num, NULL, FALSE);
    }
    if (lpCaption != NULL)
    {
        int num = WideCharToMultiByte(CP_OEMCP, NULL, lpCaption, -1, NULL, 0, NULL, FALSE);
        WideCharToMultiByte(CP_OEMCP, NULL, lpCaption, -1, sendInfo.argValue[2], num, NULL, FALSE);
    }

    sprintf_s(sendInfo.argValue[3], "%08X", uType);

    // 把需要发送的信息复制到共享内存中
    WaitForSingleObject(hWriteSemaphore, INFINITE);
    memcpy(lpBase, &sendInfo, sizeof(info));
    // V操作 信号量+1 使得注射器能够获取共享内存中的信息
    ReleaseSemaphore(hReadSemaphore, 1, NULL);
    return OldMessageBoxW(hWnd, lpText, lpCaption, uType);
}

/*** 任务3：实现文件操作 API 截获 ***/

/*** 文件创建 CreateFile ***/
static HANDLE(WINAPI* OldCreateFile)(
    LPCTSTR lpFileName,                          // 文件名
    DWORD dwDesiredAccess,                       // 访问模式
    DWORD dwShareMode,                           // 共享模式
    LPSECURITY_ATTRIBUTES lpSecurityAttributes,  // 安全属性（也即销毁方式）
    DWORD dwCreationDisposition,                 // how to create
    DWORD dwFlagsAndAttributes,                  // 文件属性
    HANDLE hTemplateFile                         // 模板文件句柄
    ) = CreateFile;

extern "C" __declspec(dllexport)HANDLE WINAPI NewCreateFile(
    LPCTSTR lpFileName,                          // 文件名
    DWORD dwDesiredAccess,                       // 访问模式
    DWORD dwShareMode,                           // 共享模式
    LPSECURITY_ATTRIBUTES lpSecurityAttributes,  // 安全属性（也即销毁方式）
    DWORD dwCreationDisposition,                 // how to create
    DWORD dwFlagsAndAttributes,                  // 文件属性
    HANDLE hTemplateFile                         // 模板文件句柄
)
{
    sendInfo.type = FUNC_CREATEFILE;
    GetLocalTime(&sendInfo.st);
    // 需要输出七个参数
    sendInfo.argNum = 7;
    // 参数名称
    sprintf_s(sendInfo.argName[0], "lpFileName");
    sprintf_s(sendInfo.argName[1], "dwDesiredAccess");
    sprintf_s(sendInfo.argName[2], "dwShareMode");
    sprintf_s(sendInfo.argName[3], "lpSecurityAttributes");
    sprintf_s(sendInfo.argName[4], "dwCreationDisposition");
    sprintf_s(sendInfo.argName[5], "dwFlagsAndAttributes");
    sprintf_s(sendInfo.argName[6], "hTemplateFile");

    // 参数值
    if (lpFileName != NULL)
    {
        int num = WideCharToMultiByte(CP_OEMCP, NULL, lpFileName, -1, NULL, 0, NULL, FALSE);
        WideCharToMultiByte(CP_OEMCP, NULL, lpFileName, -1, sendInfo.argValue[0], num, NULL, FALSE);
    }
    sprintf_s(sendInfo.argValue[1], "%08X", dwDesiredAccess);
    sprintf_s(sendInfo.argValue[2], "%08X", dwShareMode);
    sprintf_s(sendInfo.argValue[3], "%08X", lpSecurityAttributes);
    sprintf_s(sendInfo.argValue[4], "%08X", dwCreationDisposition);
    sprintf_s(sendInfo.argValue[5], "%08X", dwFlagsAndAttributes);
    sprintf_s(sendInfo.argValue[6], "%08X", hTemplateFile);

    // 把需要发送的信息复制到共享内存中
    WaitForSingleObject(hWriteSemaphore, INFINITE);
    memcpy(lpBase, &sendInfo, sizeof(info));
    // V操作 信号量+1 使得注射器能够获取共享内存中的信息
    ReleaseSemaphore(hReadSemaphore, 1, NULL);
    return OldCreateFile(lpFileName, dwDesiredAccess, dwShareMode, lpSecurityAttributes, dwCreationDisposition, dwFlagsAndAttributes, hTemplateFile);
}

/*** 文件打开 OpenFile ***/
static HFILE(WINAPI* OldOpenFile)(
    LPCSTR lpFileName,                           // 文件名
    LPOFSTRUCT lpReOpenBuff,                     // 指向存放文件信息的结构体指针
    UINT uStyle                                  // The action to be taken.
    ) = OpenFile;

extern "C" __declspec(dllexport)HFILE WINAPI NewOpenFile(
    LPCSTR lpFileName,                           // 文件名
    LPOFSTRUCT lpReOpenBuff,                     // 指向存放文件信息的结构体指针
    UINT uStyle                                  // The action to be taken.
)
{
    sendInfo.type = FUNC_OPENFILE;
    GetLocalTime(&sendInfo.st);
    // 需要输出三个参数
    sendInfo.argNum = 3;
    // 参数名称
    sprintf_s(sendInfo.argName[0], "lpFileName");
    sprintf_s(sendInfo.argName[1], "lpReOpenBuff");
    sprintf_s(sendInfo.argName[2], "uStyle");

    // 参数值
    if (lpFileName != NULL)
    {
        sprintf_s(sendInfo.argValue[0], lpFileName);
    }
    sprintf_s(sendInfo.argValue[1], "%08X", lpReOpenBuff);
    sprintf_s(sendInfo.argValue[2], "%08X", uStyle);

    // 把需要发送的信息复制到共享内存中
    WaitForSingleObject(hWriteSemaphore, INFINITE);
    memcpy(lpBase, &sendInfo, sizeof(info));
    // V操作 信号量+1 使得注射器能够获取共享内存中的信息
    ReleaseSemaphore(hReadSemaphore, 1, NULL);

    return OldOpenFile(lpFileName, lpReOpenBuff, uStyle);
}

/*** 文件读 ReadFile ***/
static BOOL (WINAPI *OldReadFile)(
    HANDLE       hFile,                 // A handle to the device
    LPVOID       lpBuffer,              // A pointer to the buffer that receives the data read from a file or device
    DWORD        nNumberOfBytesToRead,  // The maximum number of bytes to be read
    LPDWORD      lpNumberOfBytesRead,   // A pointer to the variable that receives the number of bytes read when using a synchronous hFile parameter
    LPOVERLAPPED lpOverlapped           // A pointer to an OVERLAPPED structure is required if the hFile parameter was opened with FILE_FLAG_OVERLAPPED, otherwise it can be NULL.
) = ReadFile;
extern "C" __declspec(dllexport)BOOL WINAPI NewReadFile(
    HANDLE       hFile,                 // A handle to the device
    LPVOID       lpBuffer,              // A pointer to the buffer that receives the data read from a file or device
    DWORD        nNumberOfBytesToRead,  // The maximum number of bytes to be read
    LPDWORD      lpNumberOfBytesRead,   // A pointer to the variable that receives the number of bytes read when using a synchronous hFile parameter
    LPOVERLAPPED lpOverlapped           // A pointer to an OVERLAPPED structure is required if the hFile parameter was opened with FILE_FLAG_OVERLAPPED, otherwise it can be NULL.
)
{
    if (GetFileType(hFile) == FILE_TYPE_DISK) // 判断HANDLE为文件类型
    {
        sendInfo.type = FUNC_READFILE;
        GetLocalTime(&sendInfo.st);
        // 需要输出五个参数
        sendInfo.argNum = 5;
        // 参数名称
        sprintf_s(sendInfo.argName[0], "hFile");
        sprintf_s(sendInfo.argName[1], "lpBuffer");
        sprintf_s(sendInfo.argName[2], "nNumberOfBytesToRead");
        sprintf_s(sendInfo.argName[3], "lpNumberOfBytesRead");
        sprintf_s(sendInfo.argName[4], "lpOverlapped");

        // 参数值
        sprintf_s(sendInfo.argValue[0], "%08X", hFile);
        sprintf_s(sendInfo.argValue[1], "%08X", lpBuffer);
        sprintf_s(sendInfo.argValue[2], "%08X", nNumberOfBytesToRead);
        sprintf_s(sendInfo.argValue[3], "%08X", lpNumberOfBytesRead);
        sprintf_s(sendInfo.argValue[4], "%08X", lpOverlapped);

        // 把需要发送的信息复制到共享内存中
        WaitForSingleObject(hWriteSemaphore, INFINITE);
        memcpy(lpBase, &sendInfo, sizeof(info));
        // V操作 信号量+1 使得注射器能够获取共享内存中的信息
        ReleaseSemaphore(hReadSemaphore, 1, NULL);
    }

    return OldReadFile(hFile, lpBuffer, nNumberOfBytesToRead, lpNumberOfBytesRead, lpOverlapped);
}

/*** 文件写 WriteFile ***/
static BOOL(WINAPI* OldWriteFile)( // 参数含义类似于ReadFile，不再赘述
    HANDLE       hFile,
    LPCVOID      lpBuffer,
    DWORD        nNumberOfBytesToWrite,
    LPDWORD      lpNumberOfBytesWritten,
    LPOVERLAPPED lpOverlapped
    ) = WriteFile;
extern "C" __declspec(dllexport)BOOL WINAPI NewWriteFile(
    HANDLE       hFile,
    LPCVOID      lpBuffer,
    DWORD        nNumberOfBytesToWrite,
    LPDWORD      lpNumberOfBytesWritten,
    LPOVERLAPPED lpOverlapped
)
{

    if (GetFileType(hFile) == FILE_TYPE_DISK) // 判断HANDLE为文件类型
    {
        sendInfo.type = FUNC_WRITEFILE;
        GetLocalTime(&sendInfo.st);
        // 需要输出五个参数
        sendInfo.argNum = 5;
        // 参数名称
        sprintf_s(sendInfo.argName[0], "hFile");
        sprintf_s(sendInfo.argName[1], "lpBuffer");
        sprintf_s(sendInfo.argName[2], "nNumberOfBytesToWrite");
        sprintf_s(sendInfo.argName[3], "lpNumberOfBytesWritten");
        sprintf_s(sendInfo.argName[4], "lpOverlapped");

        // 参数值
        sprintf_s(sendInfo.argValue[0], "%08X", hFile);
        sprintf_s(sendInfo.argValue[1], "%08X", lpBuffer);
        sprintf_s(sendInfo.argValue[2], "%08X", nNumberOfBytesToWrite);
        sprintf_s(sendInfo.argValue[3], "%08X", lpNumberOfBytesWritten);
        sprintf_s(sendInfo.argValue[4], "%08X", lpOverlapped);

        // 把需要发送的信息复制到共享内存中
        WaitForSingleObject(hWriteSemaphore, INFINITE);
        memcpy(lpBase, &sendInfo, sizeof(info));
        // V操作 信号量+1 使得注射器能够获取共享内存中的信息
        ReleaseSemaphore(hReadSemaphore, 1, NULL);
    }
    return OldWriteFile(hFile, lpBuffer, nNumberOfBytesToWrite, lpNumberOfBytesWritten, lpOverlapped);
}

/*** 关闭文件 CloseHandle ***/
static BOOL(WINAPI* OldCloseHandle)(HANDLE hObject) = CloseHandle;
extern "C" __declspec(dllexport)BOOL WINAPI NewCloseHandle(HANDLE hObject)
{
    // 如果hObject是文件才输出日志
    if (GetFileType(hObject) == FILE_TYPE_DISK)
    {
        sendInfo.type = FUNC_CLOSEHANDLE;
        GetLocalTime(&sendInfo.st);
        // 需要输出一个参数
        sendInfo.argNum = 1;
        // 参数名称
        sprintf_s(sendInfo.argName[0], "hObject");

        // 参数值
        sprintf_s(sendInfo.argValue[0], "%08X", hObject);

        // 把需要发送的信息复制到共享内存中
        WaitForSingleObject(hWriteSemaphore, INFINITE);
        memcpy(lpBase, &sendInfo, sizeof(info));
        // V操作 信号量+1 使得注射器能够获取共享内存中的信息
        ReleaseSemaphore(hReadSemaphore, 1, NULL);
    }
    return OldCloseHandle(hObject);
}

/*** 任务2：实现堆操作 API 截获 ***/
// 堆操作 HeapCreate HeapDestroy HeapAlloc HeapFree
static HANDLE(WINAPI* OldHeapCreate)(DWORD fIOoptions, SIZE_T dwInitialSize, SIZE_T dwMaximumSize) = HeapCreate;

extern "C" __declspec(dllexport)HANDLE WINAPI NewHeapCreate(DWORD fIOoptions, SIZE_T dwInitialSize, SIZE_T dwMaximumSize)
{
    HANDLE handle = OldHeapCreate(fIOoptions, dwInitialSize, dwMaximumSize);
    sendInfo.type = FUNC_HEAPCREATE;
    GetLocalTime(&sendInfo.st);
    // 需要输出四个参数
    sendInfo.argNum = 4;
    // 参数名称
    sprintf_s(sendInfo.argName[0], "fIOoptions");
    sprintf_s(sendInfo.argName[1], "dwInitialSize");
    sprintf_s(sendInfo.argName[2], "dwMaximumSize");
    sprintf_s(sendInfo.argName[3], "handle");

    // 参数值
    sprintf_s(sendInfo.argValue[0], "%08X", fIOoptions);
    sprintf_s(sendInfo.argValue[1], "%08X", dwInitialSize);
    sprintf_s(sendInfo.argValue[2], "%08X", dwMaximumSize);
    sprintf_s(sendInfo.argValue[3], "%08X", handle);

    // 把需要发送的信息复制到共享内存中
    WaitForSingleObject(hWriteSemaphore, INFINITE);
    memcpy(lpBase, &sendInfo, sizeof(info));
    // V操作 信号量+1 使得注射器能够获取共享内存中的信息
    ReleaseSemaphore(hReadSemaphore, 1, NULL);
    
    return handle;
}

static BOOL(WINAPI* OldHeapDestroy)(HANDLE) = HeapDestroy;

extern "C" __declspec(dllexport)BOOL WINAPI NewHeapDestroy(HANDLE hHeap)
{
    sendInfo.type = FUNC_HEAPDESTROY;
    GetLocalTime(&sendInfo.st);
    // 需要输出一个参数
    sendInfo.argNum = 1;
    // 参数名称
    sprintf_s(sendInfo.argName[0], "hHeap");

    // 参数值
    sprintf_s(sendInfo.argValue[0], "%08X", hHeap);

    // 把需要发送的信息复制到共享内存中
    WaitForSingleObject(hWriteSemaphore, INFINITE);
    memcpy(lpBase, &sendInfo, sizeof(info));
    // V操作 信号量+1 使得注射器能够获取共享内存中的信息
    ReleaseSemaphore(hReadSemaphore, 1, NULL);
    return OldHeapDestroy(hHeap);
}

static BOOL (WINAPI *OldHeapFree)(
    HANDLE                 hHeap,
    DWORD                  dwFlags,
    LPVOID lpMem
) = HeapFree;

extern "C" __declspec(dllexport)BOOL WINAPI NewHeapFree(
    HANDLE                 hHeap,
    DWORD                  dwFlags,
    LPVOID lpMem
)
{
    sendInfo.type = FUNC_HEAPFREE;
    GetLocalTime(&sendInfo.st);
    // 需要输出三个参数
    sendInfo.argNum = 3;
    // 参数名称
    sprintf_s(sendInfo.argName[0], "hHeap");
    sprintf_s(sendInfo.argName[1], "dwFlags");
    sprintf_s(sendInfo.argName[2], "lpMem");

    // 参数值
    sprintf_s(sendInfo.argValue[0], "%08X", hHeap);
    sprintf_s(sendInfo.argValue[1], "%08X", dwFlags);
    sprintf_s(sendInfo.argValue[2], "%08X", lpMem);

    // 把需要发送的信息复制到共享内存中
    WaitForSingleObject(hWriteSemaphore, INFINITE);
    memcpy(lpBase, &sendInfo, sizeof(info));
    // V操作 信号量+1 使得注射器能够获取共享内存中的信息
    ReleaseSemaphore(hReadSemaphore, 1, NULL);
    return OldHeapFree(hHeap, dwFlags, lpMem);
}

/*** 任务4 注册表操作API截获 ***/

static LSTATUS(WINAPI* OldRegCreateKeyEx)(
    HKEY                        hKey,
    LPCWSTR                     lpSubKey,
    DWORD                       Reserved,
    LPWSTR                      lpClass,
    DWORD                       dwOptions,
    REGSAM                      samDesired,
    const LPSECURITY_ATTRIBUTES lpSecurityAttributes,
    PHKEY                       phkResult,
    LPDWORD                     lpdwDisposition) = RegCreateKeyEx;

extern "C" __declspec(dllexport)LSTATUS WINAPI NewRegCreateKeyEx(
    HKEY                        hKey,
    LPCWSTR                     lpSubKey,
    DWORD                       Reserved,
    LPWSTR                      lpClass,
    DWORD                       dwOptions,
    REGSAM                      samDesired,
    const LPSECURITY_ATTRIBUTES lpSecurityAttributes,
    PHKEY                       phkResult,
    LPDWORD                     lpdwDisposition
) {
    char temp[70];
    sendInfo.argNum = 9;
    // 参数名
    sprintf_s(sendInfo.argName[0], "hKey");
    sprintf_s(sendInfo.argName[1], "lpSubKey");
    sprintf_s(sendInfo.argName[2], "Reserved");
    sprintf_s(sendInfo.argName[3], "lpClass");
    sprintf_s(sendInfo.argName[4], "dwOptions");
    sprintf_s(sendInfo.argName[5], "samDesired");
    sprintf_s(sendInfo.argName[6], "lpSecurityAttributes");
    sprintf_s(sendInfo.argName[7], "phkResult");
    sprintf_s(sendInfo.argName[8], "lpdwDisposition");
    // 参数值
    sprintf_s(sendInfo.argValue[0], "%08X", hKey);
    // 宽字节转char
    memset(temp, 0, sizeof(temp));
    WideCharToMultiByte(CP_ACP, 0, lpSubKey, wcslen(lpSubKey), temp, sizeof(temp), NULL, NULL);
    strcpy_s(sendInfo.argValue[1], temp);
    sprintf_s(sendInfo.argValue[2], "%08X", Reserved);
    sprintf_s(sendInfo.argValue[3], "%08X", lpClass);
    sprintf_s(sendInfo.argValue[4], "%08X", dwOptions);
    sprintf_s(sendInfo.argValue[5], "%08X", samDesired);
    sprintf_s(sendInfo.argValue[6], "%08X", lpSecurityAttributes);
    sprintf_s(sendInfo.argValue[7], "%08X", phkResult);
    sprintf_s(sendInfo.argValue[8], "%08X", lpdwDisposition);

    sendInfo.type = FUNC_REGCREATEKEYEX;
    GetLocalTime(&(sendInfo.st));
    // 把需要发送的信息复制到共享内存中
    WaitForSingleObject(hWriteSemaphore, INFINITE);
    memcpy(lpBase, &sendInfo, sizeof(info));
    // V操作 信号量+1 使得注射器能够获取共享内存中的信息
    ReleaseSemaphore(hReadSemaphore, 1, NULL);
    return OldRegCreateKeyEx(hKey, lpSubKey, Reserved, lpClass, dwOptions, samDesired, lpSecurityAttributes, phkResult, lpdwDisposition);
}

static LSTATUS(WINAPI* OldRegSetValueEx)(
    HKEY       hKey,
    LPCWSTR    lpValueName,
    DWORD      Reserved,
    DWORD      dwType,
    const BYTE* lpData,
    DWORD      cbData
    ) = RegSetValueEx;

extern "C" __declspec(dllexport)LSTATUS WINAPI NewRegSetValueEx(
    HKEY       hKey,
    LPCWSTR    lpValueName,
    DWORD      Reserved,
    DWORD      dwType,
    const BYTE * lpData,
    DWORD      cbData)
{
    char temp[70];
    sendInfo.argNum = 6;
    // 参数名
    sprintf_s(sendInfo.argName[0], "hKey");
    sprintf_s(sendInfo.argName[1], "lpValueName");
    sprintf_s(sendInfo.argName[2], "Reserved");
    sprintf_s(sendInfo.argName[3], "dwType");
    sprintf_s(sendInfo.argName[4], "lpData");
    sprintf_s(sendInfo.argName[5], "cbData");
    // 参数值
    sprintf_s(sendInfo.argValue[0], "%08X", hKey);
    // 宽字节转char
    memset(temp, 0, sizeof(temp));
    WideCharToMultiByte(CP_ACP, 0, lpValueName, wcslen(lpValueName), temp, sizeof(temp), NULL, NULL);
    strcpy_s(sendInfo.argValue[1], temp);
    sprintf_s(sendInfo.argValue[2], "%08X", Reserved);
    sprintf_s(sendInfo.argValue[3], "%08X", dwType);
    memset(temp, 0, sizeof(temp));
    WideCharToMultiByte(CP_ACP, 0, (LPCWCH)lpData, wcslen((LPCWCH)lpData), temp, sizeof(temp), NULL, NULL);
    strcpy_s(sendInfo.argValue[4], temp);
    //strcpy(sendInfo.argValue[4], (const char *)lpData);
    //sprintf(sendInfo.argValue[4], "%08X", lpData);
    sprintf_s(sendInfo.argValue[5], "%08X", cbData);


    sendInfo.type = FUNC_REGSETVALUEEX;
    GetLocalTime(&(sendInfo.st));
    // 把需要发送的信息复制到共享内存中
    WaitForSingleObject(hWriteSemaphore, INFINITE);
    memcpy(lpBase, &sendInfo, sizeof(info));
    // V操作 信号量+1 使得注射器能够获取共享内存中的信息
    ReleaseSemaphore(hReadSemaphore, 1, NULL);
    return OldRegSetValueEx(hKey, lpValueName, Reserved, dwType, lpData, cbData);
}

static LSTATUS(WINAPI* OldRegCloseKey)(HKEY hKey) = RegCloseKey;

extern "C" __declspec(dllexport)LSTATUS WINAPI NewRegCloseKey(HKEY hKey)
{
    sendInfo.argNum = 1;
    // 参数名
    sprintf_s(sendInfo.argName[0], "hKey");
    // 参数值
    sprintf_s(sendInfo.argValue[0], "%08X", hKey);
    sendInfo.type = FUNC_REGCLOSEKEY;
    GetLocalTime(&(sendInfo.st));
    // 把需要发送的信息复制到共享内存中
    WaitForSingleObject(hWriteSemaphore, INFINITE);
    memcpy(lpBase, &sendInfo, sizeof(info));
    // V操作 信号量+1 使得注射器能够获取共享内存中的信息
    ReleaseSemaphore(hReadSemaphore, 1, NULL);
    return OldRegCloseKey(hKey);
}

static LSTATUS(WINAPI* OldRegOpenKeyEx)(
    HKEY    hKey,
    LPCWSTR lpSubKey,
    DWORD   ulOptions,
    REGSAM  samDesired,
    PHKEY   phkResult
    ) = RegOpenKeyEx;
extern "C" __declspec(dllexport)LSTATUS WINAPI NewRegOpenKeyEx(
    HKEY    hKey,
    LPCWSTR lpSubKey,
    DWORD   ulOptions,
    REGSAM  samDesired,
    PHKEY   phkResult)
{
    char temp[70];
    sendInfo.argNum = 5;
    // 参数名
    sprintf_s(sendInfo.argName[0], "hKey");
    sprintf_s(sendInfo.argName[1], "lpSubKey");
    sprintf_s(sendInfo.argName[2], "ulOptions");
    sprintf_s(sendInfo.argName[3], "samDesired");
    sprintf_s(sendInfo.argName[4], "phkResult");
    // 参数值
    sprintf_s(sendInfo.argValue[0], "%08X", hKey);
    // 宽字节转char
    memset(temp, 0, sizeof(temp));
    WideCharToMultiByte(CP_ACP, 0, lpSubKey, wcslen(lpSubKey), temp, sizeof(temp), NULL, NULL);
    strcpy_s(sendInfo.argValue[1], temp);
    sprintf_s(sendInfo.argValue[2], "%08X", ulOptions);
    sprintf_s(sendInfo.argValue[3], "%08X", samDesired);
    sprintf_s(sendInfo.argValue[4], "%08X", phkResult);

    sendInfo.type = FUNC_REGOPENKEYEX;
    GetLocalTime(&(sendInfo.st));
    // 把需要发送的信息复制到共享内存中
    WaitForSingleObject(hWriteSemaphore, INFINITE);
    memcpy(lpBase, &sendInfo, sizeof(info));
    // V操作 信号量+1 使得注射器能够获取共享内存中的信息
    ReleaseSemaphore(hReadSemaphore, 1, NULL);
    return OldRegOpenKeyEx(hKey, lpSubKey, ulOptions, samDesired, phkResult);
}

static LSTATUS(WINAPI* OldRegDeleteValue)(
    HKEY    hKey,
    LPCWSTR lpValueName
    ) = RegDeleteValue;

extern "C" __declspec(dllexport)LSTATUS WINAPI NewRegDeleteValue(
    HKEY    hKey,
    LPCWSTR lpValueName)
{
    char temp[70];
    sendInfo.argNum = 2;
    // 参数名
    sprintf_s(sendInfo.argName[0], "hKey");
    sprintf_s(sendInfo.argName[1], "lpValueName");
    // 参数值
    sprintf_s(sendInfo.argValue[0], "%08X", hKey);
    // 宽字节转char
    memset(temp, 0, sizeof(temp));
    WideCharToMultiByte(CP_ACP, 0, lpValueName, wcslen(lpValueName), temp, sizeof(temp), NULL, NULL);
    strcpy_s(sendInfo.argValue[1], temp);
    sendInfo.type = FUNC_REGDELETEVALUE;
    GetLocalTime(&(sendInfo.st));
    // 把需要发送的信息复制到共享内存中
    WaitForSingleObject(hWriteSemaphore, INFINITE);
    memcpy(lpBase, &sendInfo, sizeof(info));
    // V操作 信号量+1 使得注射器能够获取共享内存中的信息
    ReleaseSemaphore(hReadSemaphore, 1, NULL);
    return OldRegDeleteValue(hKey, lpValueName);
}


/*** 任务4 code_end ***/

/*** 任务13 对网络传输操作的截获 ***/
// SOCKET
static SOCKET(WINAPI* Oldsocket)(
    int af,
    int type,
    int protocol
    ) = socket;
extern "C" __declspec(dllexport)SOCKET WINAPI Newsocket(
    int af,
    int type,
    int protocol
    )
{
    sendInfo.type = FUNC_SOCKET;
    GetLocalTime(&sendInfo.st);
    // 需要输出三个参数
    sendInfo.argNum = 3;
    // 参数名称
    sprintf_s(sendInfo.argName[0], "af");
    sprintf_s(sendInfo.argName[1], "type");
    sprintf_s(sendInfo.argName[2], "protocol");

    // 参数值
    sprintf_s(sendInfo.argValue[0], "%08X", af);
    sprintf_s(sendInfo.argValue[1], "%08X", type);
    sprintf_s(sendInfo.argValue[2], "%08X", protocol);

    // 把需要发送的信息复制到共享内存中
    WaitForSingleObject(hWriteSemaphore, INFINITE);
    memcpy(lpBase, &sendInfo, sizeof(info));
    // V操作 信号量+1 使得注射器能够获取共享内存中的信息
    ReleaseSemaphore(hReadSemaphore, 1, NULL);
    return Oldsocket(af, type, protocol);
}

// BIND
static int(WINAPI* Oldbind)(
    SOCKET         s,
    const sockaddr* addr,
    int            namelen
    ) = bind;
extern "C" __declspec(dllexport)int WINAPI Newbind(
    SOCKET         s,
    const sockaddr * addr,
    int            namelen
)
{
    sendInfo.type = FUNC_BIND;
    GetLocalTime(&sendInfo.st);
    // 需要输出五个参数
    sendInfo.argNum = 5;
    // 参数名称
    sprintf_s(sendInfo.argName[0], "s");
    sprintf_s(sendInfo.argName[1], "addr");
    sprintf_s(sendInfo.argName[2], "namelen");

    sockaddr_in* addr_in = (sockaddr_in*)addr;
    char ipStr[INET_ADDRSTRLEN];
    char* result = (char *)inet_ntop(AF_INET, &(addr_in->sin_addr), ipStr, sizeof(ipStr));
    sprintf_s(sendInfo.argName[3], "port");
    sprintf_s(sendInfo.argName[4], "ip");
    

    // 参数值
    sprintf_s(sendInfo.argValue[0], "%08X", s);
    sprintf_s(sendInfo.argValue[1], "%08X", addr);
    sprintf_s(sendInfo.argValue[2], "%08X", namelen);
    sprintf_s(sendInfo.argValue[3], "%d", ntohs(addr_in->sin_port));
    sprintf_s(sendInfo.argValue[4], "%s", result);

    // 把需要发送的信息复制到共享内存中
    WaitForSingleObject(hWriteSemaphore, INFINITE);
    memcpy(lpBase, &sendInfo, sizeof(info));
    // V操作 信号量+1 使得注射器能够获取共享内存中的信息
    ReleaseSemaphore(hReadSemaphore, 1, NULL);
    return Oldbind(s, addr, namelen);
}

// SEND
static int(WINAPI* Oldsend)(
    SOCKET     s,
    const char* buf,
    int        len,
    int        flags
    ) = send;
extern "C" __declspec(dllexport)int WINAPI Newsend(
    SOCKET     s,
    const char* buf,
    int        len,
    int        flags
)
{
    sendInfo.type = FUNC_SEND;
    GetLocalTime(&sendInfo.st);
    // 需要输出四个参数
    sendInfo.argNum = 4;
    // 参数名称
    sprintf_s(sendInfo.argName[0], "s");
    sprintf_s(sendInfo.argName[1], "buf");
    sprintf_s(sendInfo.argName[2], "len");
    sprintf_s(sendInfo.argName[3], "flags");

    // 参数值
    sprintf_s(sendInfo.argValue[0], "%08X", s);
    sprintf_s(sendInfo.argValue[1], "%08X", buf);
    sprintf_s(sendInfo.argValue[2], "%08X", len);
    sprintf_s(sendInfo.argValue[3], "%08X", flags);

    // 把需要发送的信息复制到共享内存中
    WaitForSingleObject(hWriteSemaphore, INFINITE);
    memcpy(lpBase, &sendInfo, sizeof(info));
    // V操作 信号量+1 使得注射器能够获取共享内存中的信息
    ReleaseSemaphore(hReadSemaphore, 1, NULL);
    return Oldsend(s, buf, len, flags);
}

// CONNECT
static int(WINAPI* Oldconnect)(
    SOCKET         s,
    const sockaddr* name,
    int            namelen
    ) = connect;
extern "C" __declspec(dllexport)int WINAPI Newconnect(
    SOCKET         s,
    const sockaddr * name,
    int            namelen
)
{
    sendInfo.type = FUNC_CONNECT;
    GetLocalTime(&sendInfo.st);
    // 需要输出三个参数
    sendInfo.argNum = 3;
    // 参数名称
    sprintf_s(sendInfo.argName[0], "s");
    sprintf_s(sendInfo.argName[1], "name");
    sprintf_s(sendInfo.argName[2], "namelen");

    // 参数值
    sprintf_s(sendInfo.argValue[0], "%08X", s);
    sprintf_s(sendInfo.argValue[1], "%08X", name);
    sprintf_s(sendInfo.argValue[2], "%08X", namelen);

    // 把需要发送的信息复制到共享内存中
    WaitForSingleObject(hWriteSemaphore, INFINITE);
    memcpy(lpBase, &sendInfo, sizeof(info));
    // V操作 信号量+1 使得注射器能够获取共享内存中的信息
    ReleaseSemaphore(hReadSemaphore, 1, NULL);
    return Oldconnect(s, name, namelen);
}

// RECV
static int(WINAPI* Oldrecv)(
    SOCKET s,
    char* buf,
    int    len,
    int    flags
    ) = recv;
extern "C" __declspec(dllexport)int WINAPI Newrecv(
    SOCKET s,
    char* buf,
    int    len,
    int    flags
)
{
    sendInfo.type = FUNC_RECV;
    GetLocalTime(&sendInfo.st);
    // 需要输出四个参数
    sendInfo.argNum = 4;
    // 参数名称
    sprintf_s(sendInfo.argName[0], "s");
    sprintf_s(sendInfo.argName[1], "buf");
    sprintf_s(sendInfo.argName[2], "len");
    sprintf_s(sendInfo.argName[3], "flags");

    // 参数值
    sprintf_s(sendInfo.argValue[0], "%08X", s);
    sprintf_s(sendInfo.argValue[1], "%08X", buf);
    sprintf_s(sendInfo.argValue[2], "%08X", len);
    sprintf_s(sendInfo.argValue[3], "%08X", flags);

    // 把需要发送的信息复制到共享内存中
    WaitForSingleObject(hWriteSemaphore, INFINITE);
    memcpy(lpBase, &sendInfo, sizeof(info));
    // V操作 信号量+1 使得注射器能够获取共享内存中的信息
    ReleaseSemaphore(hReadSemaphore, 1, NULL);
    return Oldrecv(s, buf, len, flags);
}


BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
    {
        DisableThreadLibraryCalls(hModule);
        DetourTransactionBegin();

        DetourUpdateThread(GetCurrentThread());
        // 任务1
        DetourAttach(&(PVOID&)OldMessageBoxW, NewMessageBoxW);
        DetourAttach(&(PVOID&)OldMessageBoxA, NewMessageBoxA);
        
        // 任务2
        DetourAttach(&(PVOID&)OldHeapCreate, NewHeapCreate);
        DetourAttach(&(PVOID&)OldHeapDestroy, NewHeapDestroy);
        DetourAttach(&(PVOID&)OldHeapFree, NewHeapFree);
        // 任务3
        DetourAttach(&(PVOID&)OldCreateFile, NewCreateFile);     
        DetourAttach(&(PVOID&)OldOpenFile, NewOpenFile);
        DetourAttach(&(PVOID&)OldReadFile, NewReadFile);
        DetourAttach(&(PVOID&)OldWriteFile, NewWriteFile);
        DetourAttach(&(PVOID&)OldCloseHandle, NewCloseHandle);
        
        // 任务4
        DetourAttach(&(PVOID&)OldRegCreateKeyEx, NewRegCreateKeyEx);
        DetourAttach(&(PVOID&)OldRegSetValueEx, NewRegSetValueEx);
        DetourAttach(&(PVOID&)OldRegCloseKey, NewRegCloseKey);
        DetourAttach(&(PVOID&)OldRegOpenKeyEx, NewRegOpenKeyEx);
        DetourAttach(&(PVOID&)OldRegDeleteValue, NewRegDeleteValue);

        // 任务13
        DetourAttach(&(PVOID&)Oldsocket, Newsocket);
        DetourAttach(&(PVOID&)Oldbind, Newbind);
        DetourAttach(&(PVOID&)Oldsend, Newsend);
        DetourAttach(&(PVOID&)Oldconnect, Newconnect);
        DetourAttach(&(PVOID&)Oldrecv, Newrecv);
        
        DetourTransactionCommit();
    }
    break;
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        DetourTransactionBegin();
        // 任务1
        DetourDetach(&(PVOID&)OldMessageBoxW, NewMessageBoxW);
        DetourDetach(&(PVOID&)OldMessageBoxA, NewMessageBoxA);
        
        // 任务2
        DetourDetach(&(PVOID&)OldHeapCreate, NewHeapCreate);
        DetourDetach(&(PVOID&)OldHeapDestroy, NewHeapDestroy);
        DetourDetach(&(PVOID&)OldHeapFree, NewHeapFree);
        // 任务3
        DetourDetach(&(PVOID&)OldCreateFile, NewCreateFile);
        DetourDetach(&(PVOID&)OldOpenFile, NewOpenFile);     
        DetourDetach(&(PVOID&)OldReadFile, NewReadFile);
        DetourDetach(&(PVOID&)OldWriteFile, NewWriteFile);
        DetourDetach(&(PVOID&)OldCloseHandle, NewCloseHandle);
        
        //任务4
        DetourDetach(&(PVOID&)OldRegCreateKeyEx, NewRegCreateKeyEx);
        DetourDetach(&(PVOID&)OldRegSetValueEx, NewRegSetValueEx);
        DetourDetach(&(PVOID&)OldRegCloseKey, NewRegCloseKey);
        DetourDetach(&(PVOID&)OldRegOpenKeyEx, NewRegOpenKeyEx);
        DetourDetach(&(PVOID&)OldRegDeleteValue, NewRegDeleteValue);

        // 任务13
        DetourDetach(&(PVOID&)Oldsocket, Newsocket);
        DetourDetach(&(PVOID&)Oldbind, Newbind);
        DetourDetach(&(PVOID&)Oldsend, Newsend);
        DetourDetach(&(PVOID&)Oldconnect, Newconnect);
        DetourDetach(&(PVOID&)Oldrecv, Newrecv);

        DetourTransactionCommit();
        break;
    }
    return TRUE;
}

