﻿// DllInjector.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
// 负责把DllMain.cpp生成的dll文件注入到App中
/*** 调试这个程序查看效果 ***/

#include <iostream>
#include<string>
#include <cstdio>
#include <ws2tcpip.h>
#include <windows.h>
#include <detours.h>
#include <psapi.h>
#include <set>
#include <unordered_set>
#pragma comment(lib, "detours.lib")
using namespace std;
const char* AppFileName = "App1.exe";

enum TYPE {
    FUNC_MESSAGEBOXW,
    FUNC_MESSAGEBOXA,
    FUNC_HEAPCREATE,
    FUNC_HEAPDESTROY,
    FUNC_HEAPFREE,
    FUNC_CREATEFILE,
    FUNC_OPENFILE,
    FUNC_READFILE,
    FUNC_WRITEFILE,
    FUNC_CLOSEHANDLE,
    FUNC_REGCREATEKEYEX,
    FUNC_REGSETVALUEEX,
    FUNC_REGCLOSEKEY,
    FUNC_REGOPENKEYEX,
    FUNC_REGDELETEVALUE,
    FUNC_SOCKET,
    FUNC_BIND,
    FUNC_SEND,
    FUNC_CONNECT,
    FUNC_RECV
};

const char FUNC_STR[20][30] = {
    "MessageBoxW",
    "MessageBoxA",
    "HeapCreate",
    "HeapDestroy",
    "HeapFree",
    "CreateFile",
    "OpenFile",
    "ReadFile",
    "WriteFile",
    "CloseHandle",
    "RegCreateKeyEx",
    "RegSetValueEx",
    "RegCloseKey",
    "RegOpenKeyEx",
    "RegDeleteValue",
    "socket",
    "bind",
    "send",
    "connect",
    "recv"
};

// 结构体，存放需要输出的日志信息
struct info {
    int type;                      // 类型：用于区分是哪个函数发出的日志
    int argNum;                    // 参数个数：与下面的两个字符数组的第一维对应，小于10
    SYSTEMTIME st;                 // 系统时间
    char argName[10][30] = { 0 };  // 参数的名称
    char argValue[10][70] = { 0 }; // 参数的值
};
info recvInfo;

// 信号量，初始值为1，最大值为1
HANDLE hReadSemaphore = CreateSemaphore(NULL, 0, 1, L"ReadSemaphore");
HANDLE hWriteSemaphore = CreateSemaphore(NULL, 1, 1, L"WriteSemaphore");
HANDLE hMapFile = CreateFileMapping(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE, 0, sizeof(info), L"ShareMemory");
LPVOID lpBase = MapViewOfFile(hMapFile, FILE_MAP_ALL_ACCESS, 0, 0, 0); // 映射到当前代码空间

/*
* 根据进程ID判断进程是否结束
* [IN] 进程ID
* [OUT] TRUE表示进程仍然在运行，FALSE反之
*/
bool IsProcessRunning(DWORD ProcessId);

bool IsAppRunning = true;
// 线程主函数
DWORD WINAPI ThreadMain(LPVOID lpParam);

// 异常行为分析主函数
void AnalyzeMain();

// 异常行为分析子函数，由AnalyzeMain调用，分析文件操作异常行为（任务6）
void AnalyzeFileOP();

int main()
{

    
    STARTUPINFO si;
    PROCESS_INFORMATION pi;
    ZeroMemory(&si, sizeof(STARTUPINFO));
    ZeroMemory(&pi, sizeof(PROCESS_INFORMATION));
    si.cb = sizeof(STARTUPINFO);
    WCHAR DirPath[MAX_PATH + 1];
    WCHAR  DLLPath[MAX_PATH + 1];
    WCHAR PATHTMP[MAX_PATH + 1] = L"\\Dll.dll";
    wcin.getline(DirPath, MAX_PATH);
   // wcscpy_s(DirPath, MAX_PATH, L"..\\..\\hust-detours\\Dll\\Debug");         // DLL的文件夹
    wcscpy_s(DLLPath, DirPath);
    wcscat_s(DLLPath, PATHTMP);
    char szDllPath[MAX_PATH + 1];
    WideCharToMultiByte(CP_UTF8, 0, DLLPath, -1, szDllPath, MAX_PATH, NULL, NULL);
   // char DLLPath[MAX_PATH + 1] = "..\\..\\Dll\\Debug\\Dll.dll";               // DLL的地址
    WCHAR EXE[MAX_PATH + 1] = { 0 };
    wcin.getline(EXE, MAX_PATH);
    //wcscpy_s(EXE, MAX_PATH, L"..\\App\\App1\\Debug\\App1.exe");               // 用于测试的应用程序

    if (DetourCreateProcessWithDllEx(EXE, NULL, NULL, NULL, TRUE,
        CREATE_DEFAULT_ERROR_MODE | CREATE_SUSPENDED, NULL, DirPath, &si, &pi,
        szDllPath, NULL))
    {
        HANDLE hThread;
        DWORD dwThreadId;
        hThread = CreateThread(NULL, 0, ThreadMain, &pi.hProcess, 0, &dwThreadId);

        MessageBoxA(NULL, "INJECT", "INJECT", NULL);
        ResumeThread(pi.hThread);

        while (IsAppRunning)
        {
            if (WaitForSingleObject(hReadSemaphore, 100) == WAIT_OBJECT_0) {
                // 从共享内存中获取信息
                memcpy(&recvInfo, lpBase, sizeof(info));
                // 打印到控制台
                printf("**************************************\n");
                printf("DLL日志输出：%d-%d-%d %02d:%02d:%02d:%03d\n", recvInfo.st.wYear, recvInfo.st.wMonth, recvInfo.st.wDay, recvInfo.st.wHour, recvInfo.st.wMinute, recvInfo.st.wSecond, recvInfo.st.wMilliseconds);
                printf("拦截到API: %s\n", FUNC_STR[recvInfo.type]);
                printf("拦截到的参数共%d个，参数列表如下\n", recvInfo.argNum);
                for (int i = 0; i < recvInfo.argNum; i++)
                {
                    printf("%s : %s\n", recvInfo.argName[i], recvInfo.argValue[i]);
                }
                printf("**************************************\n\n");
                ReleaseSemaphore(hWriteSemaphore, 1, NULL);

                // 针对异常行为进行分析
                AnalyzeMain();
            }
            // printf("IsAppRunning:%d\n", (int)IsAppRunning);
        }
    }
    else
    {
        char error[100];
        sprintf_s(error, "%d", GetLastError());
        MessageBoxA(NULL, error, NULL, NULL);
    }
    return 0;
}

bool IsProcessRunning(DWORD ProcessId)
{
    DWORD processes[1024];
    DWORD needed;
    if (!EnumProcesses(processes, sizeof(processes), &needed)) {
        std::cerr << "Failed to enumerate processes" << std::endl;
        return false;
    }

    DWORD numProcesses = needed / sizeof(DWORD);
    for (DWORD i = 0; i < numProcesses; i++) {
        if (processes[i] == ProcessId) {
            return true;
        }
    }

    return false;
}

DWORD WINAPI ThreadMain(LPVOID lpParam) {
    HANDLE dwProcess = *(HANDLE*)lpParam;
    WaitForSingleObject(dwProcess, INFINITE);
    IsAppRunning = false;
    return 0;
}

std::unordered_set<int> heapSet;
// 异常行为分析主函数
void AnalyzeMain()
{
    unsigned num = 0;
    switch (recvInfo.type)
    {
    case FUNC_SOCKET:
        char* end;
        num = std::strtoul(recvInfo.argValue[2], &end, 10);
        if (*end != '\0')
        {
            break;
        }
        printf("协议类型：");
        switch (num)
        {
        case IPPROTO_ICMP:
            printf("ICMP");
            break;
        case IPPROTO_IGMP:
            printf("IGMP");
            break;
        case IPPROTO_TCP:
            printf("TCP");
            break;
        case IPPROTO_UDP:
            printf("UDP");
            break;
        case IPPROTO_ICMPV6:
            printf("ICMPV6");
            break;
        default:
            printf("其他类型");
            break;
        }
        printf("\n");
        break;
    case FUNC_CREATEFILE:
        AnalyzeFileOP();
        break;
    case FUNC_HEAPCREATE: {
        //cout << strtoul(recvInfo.argValue[3], NULL, 16) << endl;
        unsigned temp;
        temp = strtoul(recvInfo.argValue[3], NULL, 16);
        heapSet.insert(temp);
        //heapSet.insert(recv)
        break;
    }
    case FUNC_HEAPFREE: {
        if (heapSet.find(strtoul(recvInfo.argValue[0], NULL, 16)) == heapSet.end()) {
            //emit newInfo(QString(QLatin1String("warning: The heap repeatedly releases or releases a non-existent heap!\n")), 2);
            printf("检测到发生重复的多次释放\n");
        }
        else {
            heapSet.erase(strtoul(recvInfo.argValue[0], NULL, 16));
        }
        break;
    }
    case FUNC_REGOPENKEYEX:
    {
        //任务7-1注册表操作异常行为分析：自启动执行文件项判断
        if (strstr(recvInfo.argValue[1], "SOFTWARE\Microsoft\Windows\CurrentVersion\Run"))
        {
            //emit newInfo(QString(QLatin1String("warning: The program is modifying the startup key in the registry\n")), 2);
            printf("注册表操作异常行为分析：该注册表项为自启动执行文件项\n");
            //for (int i = 0; i < recvInfo.argNum; i++)
            //{
            //    printf("%s : %s\n", recvInfo.argName[i], recvInfo.argValue[i]);
            //}
        }

        break;
    }
    case FUNC_REGCREATEKEYEX:
    {
        printf("注册表操作异常行为分析：新增了注册表项\n");
        break;
    }
    case FUNC_REGSETVALUEEX:
    {
        printf("注册表操作异常行为分析：修改了注册表项\n");
        break;
    }
    default:
        break;
    }
}

// 异常行为分析子函数，由AnalyzeMain调用，分析文件操作异常行为（任务6）
std::set<std::string>s;
void AnalyzeFileOP()
{
    // 首先获取文件名和目录名
    std::string fullName = { recvInfo.argValue[0] };
    std::string dirName, fileName;
    char* tmp_cstr = strrchr(recvInfo.argValue[0], '\\');
    if (tmp_cstr != NULL)
    {
        std::string tmp(tmp_cstr);
        fileName = tmp.substr(1);
        dirName = fullName.substr(0, fullName.size() - tmp.size());
    }
    else
    {
        dirName = "";
        fileName = fullName;
    }

    // 判断操作范围是否有多个文件夹
    s.insert(dirName);
    if (s.size() > 1)
    {
        printf("！文件操作异常行为：操作范围有多个文件夹\n");
    }

    unsigned dwDesiredAccess = strtoul(recvInfo.argValue[1], NULL, 16);
    // 写文件
    if (dwDesiredAccess & GENERIC_WRITE)
    {
        // 修改了其它可执行代码（exe，dll，ocx）
        if (fileName.find(".exe") != std::string::npos ||
            fileName.find(".dll") != std::string::npos ||
            fileName.find(".ocx") != std::string::npos
            )
        {
            printf("！文件操作异常行为：尝试修改可执行代码\n");
        }
    }
    // 读文件
    if (dwDesiredAccess & GENERIC_READ)
    {
        // 判断是否存在自我复制的情况
        if (strcmp(fileName.c_str(), AppFileName) == 0)
        {
            printf("！文件操作异常行为：存在自我复制的情况\n");
        }
    }
}
